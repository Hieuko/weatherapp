package com.example.weatherappv2.activity.covid19;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.weatherappv2.R;
import com.example.weatherappv2.activity.weather.MainActivity;
import com.example.weatherappv2.model.Weather;
import com.example.weatherappv2.model.WorldCovid;

import org.json.JSONException;
import org.json.JSONObject;

public class MainCovidActivity extends AppCompatActivity {
    private TextView txtCases,txtRecovered,txtCritical,txtActive,txtTodayCases,txtTotalDeaths,txtTodayDeaths,txtAffectedCountries;
    private Button btnTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_covid);

        initView();

        DataCovid19 dataCovid19 = new DataCovid19();
        dataCovid19.execute();

        btnTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),CountryCovidActivity.class);
                startActivity(intent);
            }
        });
    }


    private class DataCovid19 extends AsyncTask<Void, WorldCovid, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            String url = "https://corona.lmao.ninja/v2/all/";
            WorldCovid w = new WorldCovid();
            RequestQueue requestQueue = Volley.newRequestQueue(MainCovidActivity.this);
            StringRequest request = new StringRequest(com.android.volley.Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject=new JSONObject(response);

                        String cases = jsonObject.getString("cases");
                        w.setCases(cases);

                        String recovered = jsonObject.getString("recovered");
                        w.setRecovered(recovered);

                        String critical = jsonObject.getString("critical");
                        w.setCritical(critical);

                        String active = jsonObject.getString("active");
                        w.setActive(active);

                        String todayCases = jsonObject.getString("todayCases");
                        w.setTodayCases(todayCases);

                        String deaths = jsonObject.getString("deaths");
                        w.setDeaths(deaths);

                        String todayDeaths = jsonObject.getString("todayDeaths");
                        w.setTodayDeaths(todayDeaths);

                        String affectedCountries = jsonObject.getString("affectedCountries");
                        w.setAffectedCountries(affectedCountries);

                        publishProgress(w);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("KQ",response);
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainCovidActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);
            return null;
        }

        @Override
        protected void onProgressUpdate(WorldCovid... values) {
            super.onProgressUpdate(values);
            WorldCovid worldCovid = (WorldCovid)values[0];
            txtCases.setText(worldCovid.getCases());
            txtRecovered.setText(worldCovid.getRecovered());
            txtCritical.setText(worldCovid.getCritical());
            txtActive.setText(worldCovid.getActive());
            txtTodayCases.setText(worldCovid.getTodayCases());
            txtTodayDeaths.setText(worldCovid.getTodayDeaths());
            txtTotalDeaths.setText(worldCovid.getDeaths());
            txtAffectedCountries.setText(worldCovid.getAffectedCountries());
        }
    }

    private void initView() {
        txtCases = findViewById(R.id.txtCases);
        txtRecovered = findViewById(R.id.txtRecovered);
        txtCritical = findViewById(R.id.txtCritical);
        txtActive = findViewById(R.id.txtActive);
        txtTodayCases = findViewById(R.id.txtTodayCases);
        txtTotalDeaths = findViewById(R.id.txtTotalDeaths);
        txtTodayDeaths = findViewById(R.id.txtTodayDeaths);
        txtAffectedCountries = findViewById(R.id.txtAffectedCountries);
        btnTrack = findViewById(R.id.btnTrack);
    }
}