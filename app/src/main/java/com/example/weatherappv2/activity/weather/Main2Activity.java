package com.example.weatherappv2.activity.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.weatherappv2.R;
import com.example.weatherappv2.adapter.CustomWeatherAdapter;
import com.example.weatherappv2.model.Weather;
import com.example.weatherappv2.model.WeatherCity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main2Activity extends AppCompatActivity {
    private TextView txtThanhPho;
    private ListView lvWeather;
    CustomWeatherAdapter customWeatherAdapter;
    ArrayList<WeatherCity> arrListWeather;

    String city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initView();

        Intent intent = getIntent();
        city = intent.getStringExtra("cityname");
        Api_key_48h api_key_48h = new Api_key_48h();
        if (city.equals("")) api_key_48h.execute("Ha Noi");
        else api_key_48h.execute(city);
    }


    private class Api_key_48h extends AsyncTask<String, ArrayList<WeatherCity>, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            String c = strings[0];
            String url = "https://api.openweathermap.org/data/2.5/forecast?q="+c+"&units=metric&cnt=16&lang=vi&appid=d43db6cbc621a13a6c0367d6a3d55307";
            RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject jsonObjectCity = jsonObject.getJSONObject("city");
                        String name = jsonObjectCity.getString("name");
                        city = name;
                        JSONArray jsonArrayList = jsonObject.getJSONArray("list");
                        for (int i = 0; i < jsonArrayList.length(); i++) {
                            JSONObject jsonObjectList = jsonArrayList.getJSONObject(i);

                            String day = jsonObjectList.getString("dt");
                            long dayL = Long.valueOf(day);
                            Date dateDay = new Date(dayL * 1000); //ép kiểu về mili giây
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd");
                            String daytime = simpleDateFormat.format(dateDay);

                            Date dateThu = new Date(dayL * 1000);
                            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EEEE");
                            String dayOfWeek = simpleDateFormat1.format(dateThu);

                            Date dateGio = new Date(dayL * 1000);
                            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(" HH:mm");
                            String hour = simpleDateFormat2.format(dateGio);

                            JSONObject jsonObjectMain = jsonObjectList.getJSONObject("main");
                            String max = jsonObjectMain.getString("temp_max");
                            String min = jsonObjectMain.getString("temp_min");
                            Double a = Double.valueOf(max);
                            Double b = Double.valueOf(min);
                            String tempMax = String.valueOf(a.intValue());
                            String tempMin = String.valueOf(b.intValue());
                            String humidity = jsonObjectMain.getString("humidity");

                            JSONArray jsonArrayWeather = jsonObjectList.getJSONArray("weather");
                            JSONObject jsonObjectWeather = jsonArrayWeather.getJSONObject(0);
                            String status = jsonObjectWeather.getString("description");
                            String icons = jsonObjectWeather.getString("icon");

                            //Lấy dữ liệu từ sever rồi add vào arrListWeather (16 đối tượng)
                            arrListWeather.add(new WeatherCity(daytime, dayOfWeek, hour, icons, status, tempMax + "°C", tempMin + "°C", humidity+"%"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    publishProgress(arrListWeather);
                    Log.d("KQ2", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error){
                    Toast.makeText(Main2Activity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);
            return null;
        }

        @Override
        protected void onProgressUpdate(ArrayList<WeatherCity>... values) {
            super.onProgressUpdate(values);
            arrListWeather = (ArrayList<WeatherCity>) values[0];
            txtThanhPho.setText(city);
            customWeatherAdapter.notifyDataSetChanged();
        }
    }

    private void initView()    {
        txtThanhPho = findViewById(R.id.txt_thanhpho);
        lvWeather =  findViewById(R.id.lv_weather);

        //Khởi tạo arrListWeather truyền vào đối tượng Weather
        arrListWeather = new ArrayList<WeatherCity>();

        //Khi gọi thằng customWeatherAdapter nó sẽ truyền 1 arrListWeather sau khi đã add (16 đối tượng weather) xong sang class CustomWeatherAdapter (package adapter)
        customWeatherAdapter = new CustomWeatherAdapter(Main2Activity.this,R.layout.weather_item_listview, arrListWeather);
        lvWeather.setAdapter(customWeatherAdapter);
    }
}