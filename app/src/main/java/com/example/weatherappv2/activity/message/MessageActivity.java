package com.example.weatherappv2.activity.message;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.weatherappv2.R;
import com.example.weatherappv2.adapter.MessagerAdapter;
import com.example.weatherappv2.model.Chat;
import com.example.weatherappv2.model.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageActivity extends AppCompatActivity {
    TextView txtUsername;
    CircleImageView imgUser;
    EditText edtSentMessager;
    ImageButton btnSentMessager;
    RecyclerView recyclerMessager;

    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;

    MessagerAdapter messagerAdapter;
    List<Chat> lvChat;

    ValueEventListener seenListener;

    String userID;
    String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        
        initView();

        Intent intent = getIntent();
        userID = intent.getStringExtra("userid");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("MyUsers").child(userID);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Users user = dataSnapshot.getValue(Users.class);
                txtUsername.setText(user.getUsername());

                status = user.getStatus();
                if (user.getImageURL().equals("default")) {
                    imgUser.setImageResource(R.drawable.ic_profile);
                } else {
                    Glide.with(getApplicationContext())
                            .load(user.getImageURL())
                            .into(imgUser);
                }
                readMessage(firebaseUser.getUid(), userID, user.getImageURL());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        SeenMessage(userID);

        btnSentMessager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtsent = edtSentMessager.getText().toString();
                if (!edtsent.equals("")) {
                    SentMessage(firebaseUser.getUid(), userID, edtsent);
                } else {
                    Toast.makeText(MessageActivity.this, "Vui lòng nhập tin nhắn", Toast.LENGTH_SHORT).show();
                }
                edtSentMessager.setText("");
            }
        });

    }

    private void SeenMessage(String userID){
        databaseReference = FirebaseDatabase.getInstance().getReference("Chats");
        seenListener = databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot :dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);

                    if (chat.getReceiver().equals(firebaseUser.getUid()) && chat.getSender().equals(userID)){
                        HashMap<String,Object> hashMap = new HashMap<>();
                        hashMap.put("seenSender",true);
                        hashMap.put("seenReceiver", true);
                        snapshot.getRef().updateChildren(hashMap);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void SentMessage(String sender, final String reciver, String messager) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", reciver);
        hashMap.put("messager", messager);
        hashMap.put("seenSender",true);
        hashMap.put("seenReceiver", false);

        reference.child("Chats").push().setValue(hashMap);
    }

    private void readMessage(final String myID, final String userID, final String imageURL) {
        lvChat = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference("Chats");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lvChat.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if ((chat.getReceiver().equals(myID) && chat.getSender().equals(userID)) ||
                            (chat.getReceiver().equals(userID) && chat.getSender().equals(myID))) {
                        lvChat.add(chat);
                    }
                    messagerAdapter = new MessagerAdapter(MessageActivity.this, lvChat, imageURL);
                    recyclerMessager.setAdapter(messagerAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void CheckStatus(String status){
        databaseReference = FirebaseDatabase.getInstance().getReference("MyUsers").child(firebaseUser.getUid());
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("status",status);
        databaseReference.updateChildren(hashMap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckStatus("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        databaseReference.removeEventListener(seenListener);
        CheckStatus("offline");
    }

    private void initView() {
        txtUsername = findViewById(R.id.txt_username);
        imgUser = findViewById(R.id.img_user);

        btnSentMessager = findViewById(R.id.btn_sent_messager);
        edtSentMessager = findViewById(R.id.edt_sent_messager);

        //Recycleview
        recyclerMessager = findViewById(R.id.recycle_messager);
        recyclerMessager.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerMessager.setLayoutManager(linearLayoutManager);
    }
}