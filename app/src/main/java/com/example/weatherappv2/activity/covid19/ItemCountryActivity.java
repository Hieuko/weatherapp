package com.example.weatherappv2.activity.covid19;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.weatherappv2.R;
import com.example.weatherappv2.model.CountryCovid;

public class ItemCountryActivity extends AppCompatActivity {
    TextView txtCountry,txtCases,txtRecovered,txtCritical,txtActive,txtTodayCases,txtTotalDeaths,txtTodayDeaths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_country);

        Intent intent = getIntent();
        CountryCovid countryCovid = (CountryCovid)intent.getSerializableExtra("countryCovid");

        txtCountry = findViewById(R.id.txtCountry);
        txtCases = findViewById(R.id.txtCases);
        txtRecovered = findViewById(R.id.txtRecovered);
        txtCritical = findViewById(R.id.txtCritical);
        txtActive = findViewById(R.id.txtActive);
        txtTodayCases = findViewById(R.id.txtTodayCases);
        txtTotalDeaths = findViewById(R.id.txtDeaths);
        txtTodayDeaths = findViewById(R.id.txtTodayDeaths);

        txtCountry.setText(countryCovid.getCountry());
        txtCases.setText(countryCovid.getCases());
        txtRecovered.setText(countryCovid.getRecovered());
        txtCritical.setText(countryCovid.getCritical());
        txtActive.setText(countryCovid.getActive());
        txtTodayCases.setText(countryCovid.getTodayCases());
        txtTotalDeaths.setText(countryCovid.getDeaths());
        txtTodayDeaths.setText(countryCovid.getTodayDeaths());
    }
}