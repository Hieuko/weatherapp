package com.example.weatherappv2.activity.weather;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.weatherappv2.R;
import com.example.weatherappv2.activity.covid19.MainCovidActivity;
import com.example.weatherappv2.activity.googlemap.MapsActivity;
import com.example.weatherappv2.activity.message.LoginActivity;
import com.example.weatherappv2.model.CheckConnectivityCallback;
import com.example.weatherappv2.model.MyReceiver;
import com.example.weatherappv2.model.Weather;
import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements CheckConnectivityCallback {
    View noConnectionView;
    AutoCompleteTextView edtSearch;
    ImageButton btnSearch;
    Button btnNextDay;
    TextView txtCity,txtCountry,txtTemp,txtStatus,txtTime,txtDoAm,txtApSuatKK,txtSpeedWind,txtCloud,txtTempMinMax,txtVisibility;
    ImageView imgWeather, img_navimenu;
    MyReceiver myReceiver;
    DrawerLayout drawerLayout;

    private String city;
    private SharedPreferences sharedPreferences;
    public static final String PREFS_NAME = "SFWeather";
    public static final String PREFS_SEARCH_HISTORY = "SearchHistory";
    private Set<String> history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        //Bắt sự kiện click vào img_menu , hiển thị lên Navigation
        img_navimenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);

        // Bắt sự kiện click vào item trong navigation
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_cov:
                        Intent intent = new Intent(MainActivity.this, MainCovidActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_gps:
                        Intent intent1 = new Intent(MainActivity.this, MapsActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.nav_chat:
                        Intent intent2 = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent2);
                        break;
                }
                return true;
            }
        });

        myReceiver = new MyReceiver(MainActivity.this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(myReceiver, intentFilter);

        Log.d("Saveprefs","OnCreate");
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        //getStringset trả về một tham chiếu đối tượng HashSet dc lưu trong sf
        history = sharedPreferences.getStringSet(PREFS_SEARCH_HISTORY, new HashSet<String>());

        //Sự kiện chạm vào edtSeach
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAutoCompleteSource();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ẩn bàn phím khi bấm search
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                city = edtSearch.getText().toString();
                Api_key api_key = new Api_key();
                if (city.equals("")){
                    api_key.execute("Ha Noi");
                }
                else api_key.execute(city);
                addSearchInput(edtSearch.getText().toString().trim());
            }
        });

        btnNextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String city = txtCity.getText().toString();
                if (city.equals("")|| city == null) city = "Ha Noi";
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                intent.putExtra("cityname",city);
                startActivity(intent);
            }
        });
    }

    private class Api_key extends AsyncTask<String, Weather, Void>{
        @Override
        protected Void doInBackground(String... strings) {
            String city = strings[0];
            String url = "https://api.openweathermap.org/data/2.5/weather?q="+city+"&lang=vi&appid=d43db6cbc621a13a6c0367d6a3d55307&units=metric";
            Weather weather = new Weather();
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject=new JSONObject(response);

                        String name = jsonObject.getString("name");
                        weather.setCity(name.toUpperCase());

                        JSONArray jsonArrayWeather=jsonObject.getJSONArray("weather");
                        JSONObject objectWeather=jsonArrayWeather.getJSONObject(0);
                        String status=objectWeather.getString("description");
                        weather.setStatus(status.toUpperCase());
                        String icons = objectWeather.getString("icon");
                        weather.setIcons(icons);

                        JSONObject jsonObjectMain= jsonObject.getJSONObject("main");
                        int temp=jsonObjectMain.getInt("temp");
                        weather.setTemp(temp+"");
                        int humidity = jsonObjectMain.getInt("humidity");
                        weather.setHumidity(humidity+"%");
                        int pressure = jsonObjectMain.getInt("pressure");
                        weather.setPressure(pressure+"hPa");
                        int tempMin = jsonObjectMain.getInt("temp_min");
                        int tempMax = jsonObjectMain.getInt("temp_max");
                        weather.setMinTemp(tempMin+"°C");
                        weather.setMaxTemp(tempMax+"°C");

                        JSONObject jsonObjectCloulds = jsonObject.getJSONObject("clouds");
                        int clouds = jsonObjectCloulds.getInt("all");
                        weather.setClouds(clouds+"%");

                        JSONObject jsonObjectWind = jsonObject.getJSONObject("wind");
                        double speed = jsonObjectWind.getDouble("speed");
                        weather.setWind(speed+"m/s");

                        JSONObject jsonObjectSys = jsonObject.getJSONObject("sys");
                        String country = jsonObjectSys.getString("country");
                        weather.setCountry(country);

                        int visibility = jsonObject.getInt("visibility");
                        weather.setVisibility(visibility+"m");

                        String time = jsonObject.getString("dt");
                        long l = Long.valueOf(time);
                        Date date = new Date(l*1000);// chuyển về ms
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE dd-MM-yyyy");
                        String timeDate = simpleDateFormat.format(date);
                        weather.setTime(timeDate);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    publishProgress(weather);
                    Log.d("KQ",response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);
            return null;
        }

        @Override
        protected void onProgressUpdate(Weather... values) {
            super.onProgressUpdate(values);
            Weather weather = (Weather) values[0];
            txtCity.setText(weather.getCity());
            txtStatus.setText(weather.getStatus());
            txtTemp.setText(weather.getTemp());
            txtDoAm.setText(weather.getHumidity());
            txtApSuatKK.setText(weather.getPressure());
            txtTempMinMax.setText(weather.getMinTemp()+"/"+weather.getMaxTemp());
            txtCloud.setText(weather.getClouds());
            txtSpeedWind.setText(weather.getWind());
            txtCountry.setText(weather.getCountry());
            txtVisibility.setText(weather.getVisibility());
            txtTime.setText(weather.getTime());
            Glide.with(MainActivity.this).load("https://api.openweathermap.org/img/w/"+weather.getIcons() +".png").into(imgWeather);
        }
    }

    // Hiển thị list history khi người dùng ấn vào sự kiện edtSearch
    private void setAutoCompleteSource() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, history.toArray(new String[history.size()]));
        edtSearch.setAdapter(adapter);
        edtSearch.setThreshold(1);
    }

    //Thêm dữ liệu khi người dùng nhấn vào sự kiện btnSearch
    private void addSearchInput(String input) {
        if (!history.contains(input)) {
            history.add(input);
            setAutoCompleteSource();
        }
    }

    //Hàm này dùng dễ lưu history(value) vào sf,
    //thằng history ở trên add bao nhiều thằng thì hàm này sẽ thêm vào sf bấy nhiêu giá trị
    private void savePrefs() {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putStringSet(PREFS_SEARCH_HISTORY, history);
        editor.commit();
    }

    public void onStop() {
        super.onStop();
        savePrefs();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    @Override
    public void setOnConnectivityChangeListener(boolean isOnline) {
        if (isOnline){
            noConnectionView.setVisibility(View.GONE);
            btnNextDay.setVisibility(View.VISIBLE);
            edtSearch.setVisibility(View.VISIBLE);
            btnSearch.setVisibility(View.VISIBLE);
            img_navimenu.setVisibility(View.VISIBLE);
        }else {
            noConnectionView.setVisibility(View.VISIBLE);
            btnNextDay.setVisibility(View.GONE);
            edtSearch.setVisibility(View.GONE);
            btnSearch.setVisibility(View.GONE);
            img_navimenu.setVisibility(View.GONE);
        }
    }

    private void initView() {
        edtSearch = findViewById(R.id.edt_search);
        btnSearch = findViewById(R.id.btn_seach);
        btnNextDay = findViewById(R.id.btn_next_day);
        txtCity = findViewById(R.id.txt_city);
        txtCountry = findViewById(R.id.txt_country);
        txtTemp = findViewById(R.id.txt_temp);
        txtStatus = findViewById(R.id.txt_status);
        txtTime = findViewById(R.id.txt_time);
        txtDoAm = findViewById(R.id.txt_do_am);
        txtApSuatKK = findViewById(R.id.txt_ap_suat_kk);
        txtSpeedWind = findViewById(R.id.txt_speedwind);
        txtVisibility = findViewById(R.id.txt_visiblity);
        txtTempMinMax = findViewById(R.id.txt_temp_min_max);
        txtCloud = findViewById(R.id.txt_clouds);
        imgWeather = findViewById(R.id.img_weather);
        drawerLayout = findViewById(R.id.drawer_layout);
        img_navimenu = findViewById(R.id.img_menu);
        noConnectionView = findViewById(R.id.no_connection_view);
    }
}