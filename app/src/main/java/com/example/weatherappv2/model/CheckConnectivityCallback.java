package com.example.weatherappv2.model;

public interface CheckConnectivityCallback {

    void setOnConnectivityChangeListener(boolean isOnline);
}
