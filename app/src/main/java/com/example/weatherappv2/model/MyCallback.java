package com.example.weatherappv2.model;

public interface MyCallback {
    void onCallback(String lastMessage, boolean isSeen, String idSender);
}
