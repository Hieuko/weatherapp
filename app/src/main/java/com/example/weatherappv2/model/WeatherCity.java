package com.example.weatherappv2.model;

public class WeatherCity {
    private String day;
    private String dayOfWeek;
    private String hour;
    private String icons;
    private String status;
    private String maxTemp;
    private String minTemp;
    private String humidity;

    public WeatherCity() {
    }

    public WeatherCity(String day, String dayOfWeek, String hour, String icons, String status, String maxTemp, String minTemp, String humidity) {
        this.day = day;
        this.dayOfWeek = dayOfWeek;
        this.hour = hour;
        this.icons = icons;
        this.status = status;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.humidity = humidity;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getIcons() {
        return icons;
    }

    public void setIcons(String icons) {
        this.icons = icons;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(String maxTemp) {
        this.maxTemp = maxTemp;
    }

    public String getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(String minTemp) {
        this.minTemp = minTemp;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}
