package com.example.weatherappv2.model;

public class Chat {
    private String sender;
    private String receiver;
    private String messager;
    private boolean seenSender;
    private boolean seenReceiver;

    public Chat() {
    }

    public Chat(String sender, String receiver, String messager, boolean seenSender, boolean seenReceiver) {
        this.sender = sender;
        this.receiver = receiver;
        this.messager = messager;
        this.seenSender = seenSender;
        this.seenReceiver = seenReceiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessager() {
        return messager;
    }

    public void setMessager(String messager) {
        this.messager = messager;
    }

    public boolean isSeenSender() {
        return seenSender;
    }

    public void setSeenSender(boolean seenSender) {
        this.seenSender = seenSender;
    }

    public boolean isSeenReceiver() {
        return seenReceiver;
    }

    public void setSeenReceiver(boolean seenReceiver) {
        this.seenReceiver = seenReceiver;
    }
}
