package com.example.weatherappv2.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.weatherappv2.R;
import com.example.weatherappv2.activity.message.MessageActivity;
import com.example.weatherappv2.model.Chat;
import com.example.weatherappv2.model.MyCallback;
import com.example.weatherappv2.model.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
    private Context context;
    private List<Users> mUsers;
    private String myID;

    public UsersAdapter(Context context, List<Users> users, String myID) {
        this.context = context;
        this.mUsers = users;
        this.myID = myID;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);

        return new UsersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Users users = mUsers.get(position);
        holder.txtUsername.setText(users.getUsername());

        findLastMessage(myID, users.getId(), new MyCallback() {
            @Override
            public void onCallback(String lastMessage, boolean isSeen, String idSender) {

                if (lastMessage.equals("")){
                    holder.txtLastMessage.setVisibility(View.GONE);
                }else {
                    holder.txtLastMessage.setVisibility(View.VISIBLE);
                    if (idSender.equals(myID)){
                        holder.txtLastMessage.setText("Bạn: " + lastMessage);
                        holder.txtLastMessage.setTextColor(ContextCompat.getColor(context, R.color.seenMessageColor));
                        holder.txtLastMessage.setTypeface(null, Typeface.NORMAL);
                    }else {
                        holder.txtLastMessage.setText(lastMessage);
                        if (isSeen) {
                            holder.txtLastMessage.setTextColor(ContextCompat.getColor(context, R.color.seenMessageColor));
                            holder.txtLastMessage.setTypeface(null, Typeface.NORMAL);
                        }else {
                            holder.txtLastMessage.setTextColor(ContextCompat.getColor(context, R.color.unseenMessageColor));
                            holder.txtLastMessage.setTypeface(null, Typeface.BOLD);
                        }
                    }
                }
            }
        });

        if (users.getImageURL().equals("default")) {
            holder.imgUser.setImageResource(R.drawable.ic_profile);
        } else {
            Glide.with(context)
                    .load(users.getImageURL())
                    .into(holder.imgUser);
        }

        //Status check
        if (users.getStatus().equals("online")) {
            holder.imgViewOn.setVisibility(View.VISIBLE);
            holder.imgViewOff.setVisibility(View.GONE);
        } else {
            holder.imgViewOn.setVisibility(View.GONE);
            holder.imgViewOff.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessageActivity.class);
                intent.putExtra("userid", users.getId());
                context.startActivity(intent);
            }
        });

    }

    private void findLastMessage(final String myID, final String userID, final MyCallback myCallback) {

        //Read
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Chats");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String lastMessage = "";
                boolean isSeen = false;
                String idSender = "";
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if ((chat.getReceiver().equals(myID) && chat.getSender().equals(userID)) ||
                            (chat.getReceiver().equals(userID) && chat.getSender().equals(myID))) {
                        lastMessage = chat.getMessager();
                        isSeen = chat.isSeenReceiver();
                        idSender = chat.getSender();
                    }
                }
                myCallback.onCallback(lastMessage, isSeen, idSender);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public int getItemCount() {
        if (mUsers != null) return mUsers.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtUsername;
        public ImageView imgUser;
        public ImageView imgViewOn;
        public ImageView imgViewOff;
        public TextView txtLastMessage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtUsername = itemView.findViewById(R.id.txt_user);
            imgUser = itemView.findViewById(R.id.img_user);
            imgViewOn = itemView.findViewById(R.id.img_status_onl);
            imgViewOff = itemView.findViewById(R.id.img_status_off);
            txtLastMessage = itemView.findViewById(R.id.txt_last_message);
        }
    }

}
