package com.example.weatherappv2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.weatherappv2.R;
import com.example.weatherappv2.activity.weather.MainActivity;
import com.example.weatherappv2.model.WeatherCity;

import java.util.ArrayList;
import java.util.List;


public class CustomWeatherAdapter extends ArrayAdapter<WeatherCity> {

    private Context context;
    private int resource;
    private ArrayList<WeatherCity> arrWeather;

    //Constructor(Activity,ItemListview,arrList(weather))
    public CustomWeatherAdapter(@NonNull Context context, int resource, @NonNull ArrayList<WeatherCity> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.arrWeather = objects;
    }

    public void setData(ArrayList<WeatherCity> arrWeather){
        this.arrWeather = arrWeather;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item_listview, parent, false);
        ImageView imgWeather = view.findViewById(R.id.img_weather);
        TextView txtNgay = view.findViewById(R.id.txt_ngay);
        TextView txtThu = view.findViewById(R.id.txt_thu);
        TextView txtGio = view.findViewById(R.id.txt_gio);
        TextView txtTempMinMax = view.findViewById(R.id.txt_temp_min_max);
        TextView txtStatus = view.findViewById(R.id.txt_status);
        TextView txtDoam = view.findViewById(R.id.txt_doam);

        WeatherCity weatherCity = arrWeather.get(position);
        txtNgay.setText(weatherCity.getDay());
        txtThu.setText(weatherCity.getDayOfWeek());
        txtGio.setText(weatherCity.getHour());
        txtTempMinMax.setText(weatherCity.getMaxTemp()+"/"+weatherCity.getMinTemp());
        txtStatus.setText(weatherCity.getStatus());
        txtDoam.setText(weatherCity.getHumidity());
        Glide.with(context).load("https://api.openweathermap.org/img/w/"+weatherCity.getIcons() +".png").into(imgWeather);

        return view;
    }

    @Override
    public int getCount() {
        if (arrWeather != null){
            return arrWeather.size();
        }
        return 0;
    }
}
