package com.example.weatherappv2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.weatherappv2.R;
import com.example.weatherappv2.model.Chat;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class MessagerAdapter extends RecyclerView.Adapter<MessagerAdapter.ViewHolder> {
    private Context context;
    private List<Chat> mChats;
    private String imgMessURL;

    //firebase
    FirebaseUser firebaseUser;

    public static final int MSG_TYPE_LEFT = 0;
    public static final int MSG_TYPE_RIGHT = 1;
    public String sViewType = "";

    public MessagerAdapter(Context context, List<Chat> mChats, String imgMessURL) {
        this.context = context;
        this.mChats = mChats;
        this.imgMessURL = imgMessURL;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_item_right, parent, false);
            sViewType = "right";
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_item_left, parent, false);
            sViewType = "left";
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Chat chat = mChats.get(position);
        holder.txtMessage.setText(chat.getMessager());

        if (imgMessURL.equals("default")) {
            holder.imgProfile.setImageResource(R.drawable.ic_profile);
        } else {
            Glide.with(context).load(imgMessURL).into(holder.imgProfile);
        }

        if (position==mChats.size()-1){
            if (chat.isSeenReceiver()){
                holder.txtSeen.setText("Đã xem");
            }else {
                holder.txtSeen.setText("Đã gửi");
            }
        } else {
            holder.txtSeen.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (mChats != null) return mChats.size();
        else return 0;
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mChats.get(position).getSender().equals(firebaseUser.getUid())) {
            return MSG_TYPE_RIGHT;
        } else {
            return MSG_TYPE_LEFT;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgProfile;
        public TextView txtMessage;
        public TextView txtSeen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProfile = itemView.findViewById(R.id.img_profile);
            txtMessage = itemView.findViewById(R.id.txt_message);
            txtSeen = itemView.findViewById(R.id.txt_seen);
        }
    }
}
